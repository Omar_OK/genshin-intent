package com.example.example.genshinintent;

import android.net.Uri;

import com.example.example.genshinintent.objects.CharacterG;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;

public class GenshinAPI {

    String BASE_URL = "https://genshin.jmp.blue";  // URL comú a totes les crides.
            // el convertim a String


    public ArrayList<CharacterG> getCharacters() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("characters")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }


    ArrayList<CharacterG> getWeapons() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("weapons")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

    JSONObject getCharacter(String name) throws IOException, JSONException {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("characters")
                .appendPath(name)
                .build();
        String url = builtUri.toString();

        String JsonResponse = HttpUtils.get(url);
        JSONObject jsonC = new JSONObject(JsonResponse);

            return jsonC;
    }


    private ArrayList<CharacterG> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getImageFromWiki(String name) throws IOException {

        if (name.contains(" ")){
            name = name.replaceAll("\\s+", "_");
        }
        String url ="https://genshin-impact.fandom.com/wiki/"+name ;


        Document document = Jsoup.connect(url).get();

        int i=0;
        Element image;
        String imageUrl ="";

        image = document.select("img").get(2);
        imageUrl = image.absUrl("src");
        if (imageUrl.contains(name+"_Card")){
            return imageUrl;
        }
        else if (name.equals("Traveler") && imageUrl.contains("Traveler_Male_Card")){
            return imageUrl;
        }

        if (name.equals("Traveler")){
            while (true){
            image = document.select("img").get(i);
                if (imageUrl.contains("Traveler_Male_Card")){
                    break;
                }

            }
        }
        else {

        while (true){

            if (document.select("img").get(i) == null){
                break;
            }
            image = document.select("img").get(i);

            imageUrl= image.absUrl("src");

            if (imageUrl.contains("Character_"+name+"_Card")){
                break;
            }


            i++;
        }}





        return imageUrl;

    }



    private ArrayList<CharacterG> processJson(String jsonResponse) {
        ArrayList<CharacterG> characters = new ArrayList<>();
        try {
            JSONArray jsonCharacters = new JSONArray(jsonResponse);
            for (int i = 0; i < jsonCharacters.length(); i++) {

                JSONObject jsonCharacter = getCharacter((String) jsonCharacters.get(i));

                String urlIcon;
                String urlBig;
                String urlElement;



                CharacterG character = new CharacterG();
                character.setName(jsonCharacter.getString("name"));
                character.setRarity(jsonCharacter.getString("rarity"));
                character.setVision(jsonCharacter.getString("vision"));
                character.setWeapon(jsonCharacter.getString("weapon"));
                character.setDescription(jsonCharacter.getString("description"));



                urlIcon = "https://i2.wp.com/genshinbuilds.aipurrjects.com/genshin/characters/"+character.getNameForIcon()+"/image.png?strip=all&quality=75&w=256";

                urlElement ="https://rerollcdn.com/GENSHIN/Elements/Element_"+character.getVision()+".png";

                urlBig = getImageFromWiki(character.getName());

                character.setImageIcon(urlIcon);
                character.setImageBig(urlBig);
                character.setElement(urlElement);

                characters.add(character);
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        return characters;
    }
}
