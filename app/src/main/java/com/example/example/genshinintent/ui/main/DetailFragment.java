package com.example.example.genshinintent.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.example.genshinintent.DetailActivity;
import com.example.example.genshinintent.MainActivity;
import com.example.example.genshinintent.R;
import com.example.example.genshinintent.bd.CharacterGViewModel;
import com.example.example.genshinintent.objects.CharacterG;
import com.example.example.genshinintent.databinding.DetailFragmentBinding;




public class DetailFragment extends Fragment {


    public static DetailFragment newInstance() {
        return new DetailFragment();
    }
    private View view;
    private DetailFragmentBinding binding;
    private DetailViewModel dViewModel;
    private CharacterG characterG;

    private CharacterGViewModel model;








    private DetailViewModel mViewModel;

    public DetailFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DetailFragmentBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i != null) {
            CharacterG characterG = (CharacterG) i.getSerializableExtra("Character");

            ((DetailActivity) getActivity())
                    .setActionBar(characterG.getName());
            //swOwned.setOnClickListener(characterG.setOwned(swOwned.isChecked()));

            if (characterG != null) {
                updateUi(characterG);
            }
            this.characterG = characterG;
            binding.ownedSwitch.setOnClickListener((adapter) -> {
                characterG.setOwned( binding.ownedSwitch.isChecked());
            });
            binding.carrySwitch.setOnClickListener((adapter) -> {
                characterG.setCarry( binding.carrySwitch.isChecked());
            });
            binding.supportSwitch.setOnClickListener((adapter) -> {
                characterG.setSupp( binding.supportSwitch.isChecked());
            });
            binding.healerSwitch.setOnClickListener((adapter) -> {
                characterG.setHealer( binding.healerSwitch.isChecked());
            });
        }


        return view;
    }

    private void updateUi(CharacterG characterG) {
        Log.d("MOVIE", characterG.toString());

        binding.tvNameDetail.setText(characterG.getName());
        binding.tvWeapon.setText(characterG.getWeapon());
        binding.tvDescription.setText(characterG.getDescription());

        binding.ownedSwitch.setChecked(characterG.getOwned());
        binding.carrySwitch.setChecked(characterG.getCarry());
        binding.supportSwitch.setChecked(characterG.getSupp());
        binding.healerSwitch.setChecked(characterG.getHealer());


        Glide.with(getContext()).load(
                characterG.getImageBig()
        ).into(binding.ivBigCharacter);

        Glide.with(getContext()).load(
                characterG.getElement()
        ).into(binding.ivElement);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onDestroyView() {

        model = ViewModelProviders.of(this).get(CharacterGViewModel.class);
        model.UpdateCharacterG(this.characterG);

        super.onDestroyView();
    }

}