package com.example.example.genshinintent.bd;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.example.genshinintent.GenshinAPI;
import com.example.example.genshinintent.objects.CharacterG;

import java.util.ArrayList;
import java.util.List;

public class CharacterGViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CharacterDao characterDao;
    private LiveData<List<CharacterG>> characterGLiveData;

    public CharacterGViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.characterDao = appDatabase.getCharacterGDao();
    }

    public LiveData<List<CharacterG>> getCharacterGLiveData() {
        return characterDao.getMovies();
    }


    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            /*SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            String pais = preferences.getString("pais", "es");
            String tipusConsulta = preferences.getString(
                    "tipus_consulta", "vistes"
            );*/

            GenshinAPI api = new GenshinAPI();
            ArrayList<CharacterG> result = api.getCharacters();


            characterDao.deleteCharacterGAll();
            characterDao.addCharacterGList(result);

            return null;
        }

    }

    private class UpdateCharacterTask extends AsyncTask<Void,Void,Void>{

        CharacterG characterG;

        public UpdateCharacterTask(CharacterG characterG) {
            this.characterG = characterG;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            characterDao.updateCharacterG(characterG);

            return null;
        }
    }

    public void UpdateCharacterG(CharacterG characterG){

        UpdateCharacterTask task = new UpdateCharacterTask(characterG);
        task.execute();
        //characterDao.updateCharacterG(characterG);
    }

}