package com.example.example.genshinintent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.example.example.genshinintent.objects.CharacterG;
import com.example.example.genshinintent.databinding.GvCharacterCellBinding;


import java.util.List;

public class CharacterGAdapter  extends ArrayAdapter <CharacterG> {
    public CharacterGAdapter(Context context, int resource, List<CharacterG> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CharacterG characterG = getItem(position);


        GvCharacterCellBinding binding = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.gv_character_cell,parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        } /*
        String url;
        if (characterG.getName().equals("Traveler")){
            url = "https://rerollcdn.com/GENSHIN/Characters/"+characterG.getName()+"%20(Anemo).png";
        }
        else {        url = "https://rerollcdn.com/GENSHIN/Characters/"+characterG.getName()+".png";}
        characterG.setImageIcon(url); */

        binding.tvCharMain.setText(characterG.getName());
        Glide.with(getContext()).load(characterG.getImageIcon()).into(binding.ivCharMain);
        Glide.with(getContext()).load(characterG.getElement()).into(binding.ivElementCell);


        return binding.getRoot();
    }
}