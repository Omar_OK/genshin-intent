package com.example.example.genshinintent.ui.main.ui.clist;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.example.example.genshinintent.CharacterGAdapter;
import com.example.example.genshinintent.DetailActivity;
import com.example.example.genshinintent.GenshinAPI;
import com.example.example.genshinintent.MainActivity;
import com.example.example.genshinintent.R;
import com.example.example.genshinintent.bd.CharacterGViewModel;
import com.example.example.genshinintent.databinding.MainFragmentBinding;
import com.example.example.genshinintent.objects.CharacterG;
import com.example.example.genshinintent.ui.main.NavigationActivity;

import java.util.ArrayList;

public class MainFragment extends Fragment {



    public static MainFragment newInstance() {
        return new MainFragment();
    }
    private ArrayList<CharacterG> items;
    private CharacterGAdapter adapter;
    private MainFragmentBinding binding;
    private CharacterGViewModel model;
    private MainViewModel mViewModel;



    @Override
    public void onStart() {
        super.onStart();
        //refresh();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        ((NavigationActivity) getActivity())
                .setActionBar("Genshin Intent");


        binding = null;
        binding = MainFragmentBinding.inflate(inflater);

        View view = binding.getRoot();

        items = new ArrayList<>();
        adapter = new CharacterGAdapter(getContext(),R.layout.gv_character_cell,items);



        /*
        adapter = new ArrayAdapter<>(
                getContext(),
                R.layout.gv_character_cell,
                R.id.tvCharMain,
                items
        ); */

        binding.menuGridView.setAdapter(adapter);
        binding.menuGridView.setOnItemClickListener((adapterView, view1, i, l) -> {
            CharacterG characterG = (CharacterG) adapterView.getItemAtPosition(i);

            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra("Character", characterG);

            startActivity(intent);
        });

        model = ViewModelProviders.of(this).get(CharacterGViewModel.class);
        model.getCharacterGLiveData().observe(getViewLifecycleOwner(),characterGS -> {
            adapter.clear();
            adapter.addAll(characterGS);
        });

        /*
        GridView gridview;
        gridView.setOnItemClickListener((adapter, fragment, i, l) -> {
            CharacterG characterG = (CharacterG) adapter.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra("Character", characterG);

            startActivity(intent);
        }); */

        return view;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_genshin_fragment,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh){
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void refresh() {
        /*RefreshDataTask task = new RefreshDataTask();
        task.execute();*/
        model.reload();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<CharacterG>> {
        @Override
        protected ArrayList<CharacterG> doInBackground(Void... voids) {
            GenshinAPI api = new GenshinAPI();
            ArrayList<CharacterG> result = api.getCharacters();


            return result;
        }
        @Override
        protected void onPostExecute(ArrayList<CharacterG> characterGS) {
            adapter.clear();
            for (CharacterG characterG : characterGS) {
                adapter.add(characterG);
            }
        }
    }

}