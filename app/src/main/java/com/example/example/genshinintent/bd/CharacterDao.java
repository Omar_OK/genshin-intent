package com.example.example.genshinintent.bd;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.example.genshinintent.objects.CharacterG;

import java.util.List;

@Dao
public interface CharacterDao {
    @Query("select * from characterG")
    LiveData<List<CharacterG>> getMovies();

    @Insert
    void addCharacterG(CharacterG characterG);

    @Insert
    void addCharacterGList(List<CharacterG> characterGList);

    @Delete
    void deleteCharacterG(CharacterG characterG);

    @Update
    void updateCharacterG(CharacterG characterG);

    @Query("DELETE FROM characterG")
    void deleteCharacterGAll();
}
