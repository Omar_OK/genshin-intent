package com.example.example.genshinintent.ui.main.ui.slideshow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.example.genshinintent.R;

public class WebViewFragment extends Fragment {

    private WebViewViewModel webViewViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        webViewViewModel =
                new ViewModelProvider(this).get(WebViewViewModel.class);
        View root = inflater.inflate(R.layout.fragment_webview, container, false);
        WebView wb = root.findViewById(R.id.webView1);
        wb.getSettings().setJavaScriptEnabled(true);

        wb.loadUrl("https://keqingmains.com/");
        return root;
    }
}