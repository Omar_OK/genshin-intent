package com.example.example.genshinintent.objects;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class CharacterG implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private String vision;
    private String weapon;
    private String rarity;
    private String description;

    private String element;
    private String imageBig;
    private String imageIcon;



    private Boolean owned = false;
    private Boolean carry = false;
    private Boolean supp = false;
    private Boolean healer = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public String getNameForIcon() {
        if(name.contains("Traveler")){
            return "traveler_anemo";
        }
        else {

            return name.replaceAll("\\s+", "_").toLowerCase();

        }
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(String imageIcon) {
        this.imageIcon = imageIcon;
    }

    public Boolean getOwned() {
        return owned;
    }

    public void setOwned(Boolean owned) {
        this.owned = owned;
    }

    public Boolean getCarry() {
        return carry;
    }

    public void setCarry(Boolean carry) {
        this.carry = carry;
    }

    public Boolean getSupp() {
        return supp;
    }

    public void setSupp(Boolean supp) {
        this.supp = supp;
    }

    public Boolean getHealer() {
        return healer;
    }

    public void setHealer(Boolean healer) {
        this.healer = healer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getImageBig() {
        return imageBig;
    }

    public void setImageBig(String imageBig) {
        this.imageBig = imageBig;
    }

    private String istrue(boolean bol){
        if (bol){
            return "Yes";
        }
        else return "No";
    }

    @Override
    public String toString() {
        return "CharacterG{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", vision='" + vision + '\'' +
                ", weapon='" + weapon + '\'' +
                ", rarity='" + rarity + '\'' +
                ", description='" + description + '\'' +
                ", element='" + element + '\'' +
                ", imageBig='" + imageBig + '\'' +
                ", imageIcon='" + imageIcon + '\'' +
                ", owned=" + owned +
                ", carry=" + carry +
                ", supp=" + supp +
                ", healer=" + healer +
                '}';
    }

    public CharacterG() {
    }
}
