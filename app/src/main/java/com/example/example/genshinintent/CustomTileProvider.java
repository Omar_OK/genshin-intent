package com.example.example.genshinintent;

import android.content.res.AssetManager;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomTileProvider implements TileProvider {

    private static final int TILE_WIDTH = 256;
    private static final int TILE_HEIGHT = 256;
    private static final int BUFFER_SIZE = 16 * 1024;

    private final AssetManager mAssetManager;

    public CustomTileProvider(AssetManager assetManager) {
        mAssetManager = assetManager;
    }



    @Nullable

    @Override
    public Tile getTile(int i, int i1, int i2) {
        byte[] image = readTileImage(i, i1, i2);

        return (image==null)
                ? TileProvider.NO_TILE
                : new Tile(TILE_WIDTH, TILE_HEIGHT, image);
    }

    private byte[] readTileImage(int x, int y, int zoom) {
        InputStream in = null;
        ByteArrayOutputStream buffer = null;

        try {
            if (zoom >= 4){
                in = mAssetManager.open(getTileFilenameZoom4Plus(x, y, zoom));
            }
            else {in = mAssetManager.open(getTileFilename(x, y, zoom));}

            buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[BUFFER_SIZE];

            while ((nRead = in.read(data, 0, BUFFER_SIZE)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();

            return buffer.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) try { in.close(); } catch (Exception ignored) {}
            if (buffer != null) try { buffer.close(); } catch (Exception ignored) {}
        }
    }

    private String getTileFilename(int x, int y, int zoom) {
        return "map/" + "zoom" + zoom + '/' + '_' + y + '_' + x + ".png";
    }

    private String getTileFilenameZoom4Plus(int x, int y, int zoom) {
        if (x >= 10){
            return "map/" + "zoom" + zoom + '/' + '_' + y + '_' + x + ".png";

        }
        else {
            return "map/" + "zoom" + zoom + '/' + '_' + y + "_0" + x + ".png";
        }

    }
}
